namespace GET.Controls.Icons
{
    partial class IconSelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IconSelectionForm));
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.imagesGoogleEarthPaddles = new System.Windows.Forms.ImageList(this.components);
            this.imagesFVLocalPoints = new System.Windows.Forms.ImageList(this.components);
            this.imagesNGA = new System.Windows.Forms.ImageList(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.listBoxLibraries = new System.Windows.Forms.ListBox();
            this.listViewIcons = new System.Windows.Forms.ListView();
            this.imageListActive = new System.Windows.Forms.ImageList(this.components);
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(364, 487);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 6;
            this.buttonOK.Text = "&OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(445, 487);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 7;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // imagesGoogleEarthPaddles
            // 
            this.imagesGoogleEarthPaddles.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imagesGoogleEarthPaddles.ImageStream")));
            this.imagesGoogleEarthPaddles.Tag = "http://maps.google.com/mapfiles/kml/paddle/";
            this.imagesGoogleEarthPaddles.TransparentColor = System.Drawing.Color.Transparent;
            this.imagesGoogleEarthPaddles.Images.SetKeyName(0, "z-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(1, "1.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(2, "1-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(3, "2.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(4, "2-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(5, "3.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(6, "3-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(7, "4.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(8, "4-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(9, "5.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(10, "5-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(11, "6.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(12, "6-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(13, "7.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(14, "7-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(15, "8.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(16, "8-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(17, "9.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(18, "9-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(19, "10.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(20, "10-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(21, "a.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(22, "a-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(23, "b.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(24, "blu-blank.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(25, "blu-blank-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(26, "blu-circle.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(27, "blu-circle-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(28, "blu-diamond.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(29, "blu-diamond-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(30, "blu-square.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(31, "blu-square-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(32, "blu-stars.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(33, "blu-stars-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(34, "b-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(35, "c.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(36, "c-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(37, "d.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(38, "d-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(39, "e.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(40, "e-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(41, "f.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(42, "f-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(43, "g.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(44, "g-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(45, "go.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(46, "go-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(47, "grn-blank.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(48, "grn-blank-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(49, "grn-circle.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(50, "grn-circle-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(51, "grn-diamond.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(52, "grn-diamond-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(53, "grn-square.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(54, "grn-square-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(55, "grn-stars.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(56, "grn-stars-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(57, "h.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(58, "h-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(59, "i.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(60, "i-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(61, "j.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(62, "j-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(63, "k.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(64, "k-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(65, "l.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(66, "l-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(67, "ltblu-blank.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(68, "ltblu-blank-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(69, "ltblu-circle.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(70, "ltblu-circle-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(71, "ltblu-diamond.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(72, "ltblu-diamond-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(73, "ltblu-square.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(74, "ltblu-square-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(75, "ltblu-stars.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(76, "ltblu-stars-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(77, "m.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(78, "m-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(79, "n.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(80, "n-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(81, "o.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(82, "o-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(83, "p.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(84, "pause.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(85, "pause-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(86, "pink-blank.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(87, "pink-blank-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(88, "pink-circle.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(89, "pink-circle-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(90, "pink-diamond.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(91, "pink-diamond-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(92, "pink-square.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(93, "pink-square-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(94, "pink-stars.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(95, "pink-stars-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(96, "p-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(97, "purple-circle.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(98, "purple-circle-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(99, "purple-diamond.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(100, "purple-diamond-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(101, "purple-square.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(102, "purple-square-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(103, "purple-stars.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(104, "purple-stars-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(105, "q.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(106, "q-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(107, "r.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(108, "red-circle.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(109, "red-circle-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(110, "red-diamond.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(111, "red-diamond-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(112, "red-square.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(113, "red-square-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(114, "red-stars.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(115, "red-stars-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(116, "r-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(117, "route.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(118, "s.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(119, "s-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(120, "stop.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(121, "stop-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(122, "t.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(123, "t-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(124, "u.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(125, "u-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(126, "v.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(127, "v-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(128, "w.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(129, "wht-blank.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(130, "wht-blank-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(131, "wht-circle.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(132, "wht-circle-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(133, "wht-diamond.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(134, "wht-diamond-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(135, "wht-square.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(136, "wht-square-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(137, "wht-stars.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(138, "wht-stars-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(139, "w-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(140, "x.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(141, "x-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(142, "y.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(143, "y-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(144, "ylw-blank.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(145, "ylw-blank-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(146, "ylw-circle.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(147, "ylw-circle-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(148, "ylw-diamond.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(149, "ylw-diamond-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(150, "ylw-square.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(151, "ylw-square-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(152, "ylw-stars.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(153, "ylw-stars-lv.png");
            this.imagesGoogleEarthPaddles.Images.SetKeyName(154, "z.png");
            // 
            // imagesFVLocalPoints
            // 
            this.imagesFVLocalPoints.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imagesFVLocalPoints.ImageStream")));
            this.imagesFVLocalPoints.Tag = "C:\\PFPS\\falcon\\data\\Icons\\localpnt\\";
            this.imagesFVLocalPoints.TransparentColor = System.Drawing.Color.Transparent;
            this.imagesFVLocalPoints.Images.SetKeyName(0, "yelstar.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(1, "A-10.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(2, "airplane.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(3, "AirShip.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(4, "anchor1.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(5, "anchor2.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(6, "B-1.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(7, "B-52.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(8, "B-52 white.bmp");
            this.imagesFVLocalPoints.Images.SetKeyName(9, "blue dot2.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(10, "blue IP.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(11, "blue local.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(12, "Blue Target.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(13, "blue turn.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(14, "blue_chk.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(15, "blue_crs.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(16, "blue_dot.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(17, "blue_sqr.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(18, "blue_x.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(19, "blue_x2.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(20, "blustar.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(21, "boat 1.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(22, "C-130.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(23, "car1.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(24, "Cargo.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(25, "cemeteri.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(26, "circ.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(27, "cyan dot2.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(28, "Cyan Target.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(29, "cyan_chk.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(30, "cyan_crs.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(31, "cyan_dot.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(32, "cyan_sqr.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(33, "cyan_x.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(34, "cyan_x2.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(35, "cynstar.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(36, "dot.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(37, "E-3.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(38, "F-4.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(39, "F-15.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(40, "face1.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(41, "flash1.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(42, "GndFoot.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(43, "GndMotor.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(44, "gray dot2.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(45, "gray GndFoot.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(46, "gray IP.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(47, "gray local.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(48, "gray turn.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(49, "gray x.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(50, "Gray x2.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(51, "green dot2.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(52, "gren IP.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(53, "gren local.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(54, "gren turn.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(55, "gren_chk.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(56, "gren_crs.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(57, "gren_dot.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(58, "gren_sqr.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(59, "gren_x.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(60, "gren_x2.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(61, "grn target.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(62, "grnstar.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(63, "gry Check.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(64, "gry Cross.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(65, "gry dot.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(66, "gry square.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(67, "Gry Target.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(68, "grystar.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(69, "hand1.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(70, "hazwaste.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(71, "Helicopter.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(72, "Hog.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(73, "hospital.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(74, "house1.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(75, "info.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(76, "Kc-135.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(77, "light 1.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(78, "Local Point.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(79, "Local Point IP.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(80, "Local Point Target.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(81, "Local Point Turn.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(82, "MarSwimmer.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(83, "medical.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(84, "nuke.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(85, "PARACHUT.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(86, "pistol1.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(87, "port_civ.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(88, "purp dot2.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(89, "Purp IP.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(90, "purp local.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(91, "Purp Target.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(92, "Purp turn.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(93, "purp_chk.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(94, "purp_crs.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(95, "purp_dot.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(96, "purp_sqr.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(97, "purp_x.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(98, "purp_x2.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(99, "purstar.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(100, "pushpin.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(101, "red dot2.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(102, "red GndFoot.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(103, "red IP.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(104, "red local.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(105, "red target.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(106, "red turn.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(107, "red_chk.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(108, "red_crs.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(109, "red_dot.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(110, "red_sqr.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(111, "red_x.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(112, "red_x2.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(113, "redstar.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(114, "ship1.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(115, "SubSurface.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(116, "SurfaceShip.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(117, "Tactical Airfield.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(118, "T-Bird.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(119, "tlight1.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(120, "Tornado.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(121, "tree.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(122, "truck1.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(123, "truck2.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(124, "Tweet.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(125, "Viper.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(126, "Whale.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(127, "white cross.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(128, "white dot.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(129, "white dot2.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(130, "White IP.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(131, "white local.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(132, "white square.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(133, "white star.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(134, "White Target.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(135, "white turn.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(136, "white x.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(137, "white x2.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(138, "yel check.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(139, "yel dot2.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(140, "yel IP.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(141, "yel local.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(142, "Yel target.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(143, "Yel turn.ico");
            this.imagesFVLocalPoints.Images.SetKeyName(144, "yelo_crs.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(145, "yelo_dot.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(146, "yelo_sqr.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(147, "yelo_x.ICO");
            this.imagesFVLocalPoints.Images.SetKeyName(148, "yelo_x2.ICO");
            // 
            // imagesNGA
            // 
            this.imagesNGA.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imagesNGA.ImageSize = new System.Drawing.Size(16, 16);
            this.imagesNGA.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer1.Location = new System.Drawing.Point(4, 2);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.listBoxLibraries);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.listViewIcons);
            this.splitContainer1.Size = new System.Drawing.Size(901, 467);
            this.splitContainer1.SplitterDistance = 308;
            this.splitContainer1.TabIndex = 8;
            // 
            // listBoxLibraries
            // 
            this.listBoxLibraries.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxLibraries.FormattingEnabled = true;
            this.listBoxLibraries.Location = new System.Drawing.Point(0, 0);
            this.listBoxLibraries.Name = "listBoxLibraries";
            this.listBoxLibraries.Size = new System.Drawing.Size(308, 459);
            this.listBoxLibraries.TabIndex = 0;
            this.listBoxLibraries.SelectedIndexChanged += new System.EventHandler(this.OnIconLibraryChanged);
            // 
            // listViewIcons
            // 
            this.listViewIcons.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewIcons.LargeImageList = this.imageListActive;
            this.listViewIcons.Location = new System.Drawing.Point(3, 0);
            this.listViewIcons.Name = "listViewIcons";
            this.listViewIcons.Size = new System.Drawing.Size(586, 459);
            this.listViewIcons.TabIndex = 0;
            this.listViewIcons.UseCompatibleStateImageBehavior = false;
            this.listViewIcons.SelectedIndexChanged += new System.EventHandler(this.OnIconChanged);
            this.listViewIcons.DoubleClick += new System.EventHandler(this.OnListViewDoubleClick);
            // 
            // imageListActive
            // 
            this.imageListActive.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageListActive.ImageSize = new System.Drawing.Size(32, 32);
            this.imageListActive.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // IconSelectionForm
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::GET.Controls.Properties.Resources.skyblue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(909, 522);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Name = "IconSelectionForm";
            this.Text = "Select a New Icon";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.ImageList imagesGoogleEarthPaddles;
        private System.Windows.Forms.ImageList imagesFVLocalPoints;
        private System.Windows.Forms.ImageList imagesNGA;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListBox listBoxLibraries;
        private System.Windows.Forms.ListView listViewIcons;
        private System.Windows.Forms.ImageList imageListActive;
    }
}