// CONTROL: AltitudeControl
//
// This control is used to adjust the altitude properties for a feature.  It allows
// the user to set two basic properties: the altitude and the altitude type.
//
// Public Properties:
//    Altitude                  Returns the altitude, in feet (either AGL or MSL),
//                              depending on the type of altitude selected
//    AltitudeType              Type "AltType".  Can be Fixed AGL, Fixed MSL, or
//                              Clamp to Ground -- the 3 kinds of altitude GE uses
//
// Methods:

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using GET.Core;

namespace GET.Controls
{
    public partial class AltitudeControl : UserControl
    {
        #region Private Member Data
        private int altitude;
        private AltType altitudeType;
        #endregion

        #region Public Properties
        public int Altitude
        {
            get { return altitude; }
            set
            {
                altitude = value;
                this.numericAltitude.Value = altitude;
            }
        }
        public AltType AltitudeType
        {
            get { return altitudeType; }
            set
            {
                altitudeType = value;                
                if (altitudeType == AltType.FixedMSL)
                    comboBoxType.SelectedIndex = 0;
                else if (altitudeType == AltType.FixedAGL)
                    comboBoxType.SelectedIndex = 1;
                else if (altitudeType == AltType.ClampToGround)
                    comboBoxType.SelectedIndex = 2;
            }
        }
        public override string Text
        {
            set { groupBox1.Text = value; }
        }

        public bool TypeVisible
        {
            set { comboBoxType.Visible = value; }
        }
        #endregion

        #region Methods
        public AltitudeControl()
        {
            InitializeComponent();
            Altitude = 5000;
            altitudeType = AltType.FixedMSL;
        }


        #endregion

        private void OnValidated(object sender, EventArgs e)
        {
            altitude = (int)numericAltitude.Value;
            if (comboBoxType.SelectedIndex == 0)
                altitudeType = AltType.FixedMSL;
            else if (comboBoxType.SelectedIndex == 1)
                altitudeType = AltType.FixedAGL;
            else if (comboBoxType.SelectedIndex == 2)
                altitudeType = AltType.ClampToGround;
            this.Validate();
        }
    }
}
