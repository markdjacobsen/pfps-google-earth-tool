namespace GET.Controls
{
    partial class AltitudeSelector
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxMSL = new System.Windows.Forms.TextBox();
            this.textBoxAGL = new System.Windows.Forms.TextBox();
            this.radioClamp = new System.Windows.Forms.RadioButton();
            this.radioMSL = new System.Windows.Forms.RadioButton();
            this.radioAGL = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxMSL);
            this.groupBox1.Controls.Add(this.textBoxAGL);
            this.groupBox1.Controls.Add(this.radioClamp);
            this.groupBox1.Controls.Add(this.radioMSL);
            this.groupBox1.Controls.Add(this.radioAGL);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(236, 102);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Altitude Properties";
            // 
            // textBoxMSL
            // 
            this.textBoxMSL.Location = new System.Drawing.Point(147, 43);
            this.textBoxMSL.Name = "textBoxMSL";
            this.textBoxMSL.Size = new System.Drawing.Size(74, 20);
            this.textBoxMSL.TabIndex = 4;
            this.textBoxMSL.Validated += new System.EventHandler(this.OnValidateTextBox);
            // 
            // textBoxAGL
            // 
            this.textBoxAGL.Location = new System.Drawing.Point(147, 19);
            this.textBoxAGL.Name = "textBoxAGL";
            this.textBoxAGL.Size = new System.Drawing.Size(74, 20);
            this.textBoxAGL.TabIndex = 3;
            this.textBoxAGL.Validated += new System.EventHandler(this.OnValidateTextBox);
            // 
            // radioClamp
            // 
            this.radioClamp.AutoSize = true;
            this.radioClamp.Location = new System.Drawing.Point(11, 67);
            this.radioClamp.Name = "radioClamp";
            this.radioClamp.Size = new System.Drawing.Size(104, 17);
            this.radioClamp.TabIndex = 2;
            this.radioClamp.TabStop = true;
            this.radioClamp.Text = "Clamp to Ground";
            this.radioClamp.UseVisualStyleBackColor = true;
            this.radioClamp.CheckedChanged += new System.EventHandler(this.OnAltitudeTypeChanged);
            // 
            // radioMSL
            // 
            this.radioMSL.AutoSize = true;
            this.radioMSL.Location = new System.Drawing.Point(11, 44);
            this.radioMSL.Name = "radioMSL";
            this.radioMSL.Size = new System.Drawing.Size(130, 17);
            this.radioMSL.TabIndex = 1;
            this.radioMSL.TabStop = true;
            this.radioMSL.Text = "Constant MSL Altitude";
            this.radioMSL.UseVisualStyleBackColor = true;
            this.radioMSL.CheckedChanged += new System.EventHandler(this.OnAltitudeTypeChanged);
            // 
            // radioAGL
            // 
            this.radioAGL.AutoSize = true;
            this.radioAGL.Location = new System.Drawing.Point(11, 20);
            this.radioAGL.Name = "radioAGL";
            this.radioAGL.Size = new System.Drawing.Size(129, 17);
            this.radioAGL.TabIndex = 0;
            this.radioAGL.TabStop = true;
            this.radioAGL.Text = "Constant AGL Altitude";
            this.radioAGL.UseVisualStyleBackColor = true;
            this.radioAGL.CheckedChanged += new System.EventHandler(this.OnAltitudeTypeChanged);
            // 
            // AltitudeSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "AltitudeSelector";
            this.Size = new System.Drawing.Size(236, 102);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxMSL;
        private System.Windows.Forms.TextBox textBoxAGL;
        private System.Windows.Forms.RadioButton radioClamp;
        private System.Windows.Forms.RadioButton radioMSL;
        private System.Windows.Forms.RadioButton radioAGL;
    }
}
