/********************************************************************************/
/* Class:   GET.Core.Project                                                    */
/*                                                                              */
/* A single Google Earth Tool (.GET) project.  A Project is an intermediate     */
/* format between PFPS and Google Earth .KML.  Each project may contain any     */
/* number of objects and styles.  Most of the data fields in this object        */
/* describe the author and authoring organization.  This is in accordance with  */
/* DOD "best practices" for SIPRNET GE, which emphasize having good             */
/* accountability for who creates and maintains .KML files.                     */
/*                                                                              */
/* Public Properties:                                                           */
/*  string Author               The author of the project                       */
/*  string Classification       Classification level (secret, unclass, etc.)    */
/*  DateTime Date               The creation or version date                    */
/*  string Description          A description of the project                    */
/*  string Email                E-mail for the author or organization           */
/*  string Name                 A name/title for the project                    */
/*  string Organization         The organization that created the project       */
/*  string Phone                Phone contact info for the organization         */
/*  string Snippet              A 2-line snippet that appears in GE for .KML    */
/*  GETObject RootObject        This is the root object (almost always a        */
/*                              folder) that contains all the objects in the    */
/*                              project.  Objects are stored in a parent-child  */
/*                              architecture within this object.                */
/* List<Style> Styles           A list of reusable graphical styles.  These     */
/*                              reusable styles can be referenced by any        */
/*                              object within the project.                      */
/* string Website               The website where the .KML is maintained        */
/*                                                                              */
/* Methods:                                                                     */
/*                                                                              */
/* bool CreateKML(string filename) - IN PROGRESS                                */
/*                                                                              */
/*      This function builds a .KML out of the project.  It creates the new     */
/*      file, generates header info, exports styles, then exports the object    */
/*      hierarchy itself.                                                       */
/*                                                                              */
/* void GenerateDefaultStyles() - COMPLETE                                      */
/*                                                                              */
/*      This method generates several hard-coded styles and stores them in the  */
/*      "Styles" list.  This gives inexperienced users several pre-created      */
/*      styles for common tasks.                                                */
/*                                                                              */
/********************************************************************************/



using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using GET.Core.Objects;

namespace GET.Core
{
    [Serializable]
    public class Project
    {
        #region Private Member Data
        private string author;
        private string classification;        
        private DateTime date;     
        private string description;
        private string email;        
        private string name;
        private string organization;
        private string phone;
        private string snippet;        
        private string website;        
        #endregion

        #region Public Properties
        public string Author
        {
            get { return author; }
            set { author = value; }
        }
        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }
        public string Classification
        {
            get { return classification; }
            set { classification = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Organization
        {
            get { return organization; }
            set { organization = value; }
        }
        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }
        public string Snippet
        {
            get { return snippet; }
            set { snippet = value; }
        }
        public string Website
        {
            get { return website; }
            set { website = value; }
        }
        public GETObject RootObject;
        public List<Style> Styles = new List<Style>();
        #endregion

        #region Methods        

        public Project()
        {            
            author = "";
            date = System.DateTime.Today;
            description = "";
            name = "New Project";
            organization = "";
            snippet = "PFPS Google Earth Tool .KML";

            RootObject = new Folder();
        }

        public void GenerateDefaultStyles()
        {
            // Style 1 : White Route
            Style Style1 = new Style();
            Style1.Name = "White Route";
            Style1.LineColor = Color.White;
            Style1.LineWidth = 1.0;
            Style1.LabelColor = Color.White;
            Style1.LabelScale = 0.8;
            Style1.Extrude = false;
            Style1.LabelColor = Color.White;
            Style1.LabelOpacity = 100;
            Style1.IconColor = Color.White;
            Style1.IconOpacity = 100;
            Style1.IconScale = 0.8;

            // Style 2 : Red Airspace/Corridor
            Style Style2 = new Style();
            Style2.Name = "Red Airspace/Corridor";
            Style2.LineColor = Color.Red;
            Style2.LineWidth = 1.0;
            Style2.Extrude = true;
            Style2.LabelColor = Color.White;
            Style2.LabelOpacity = 100;
            Style2.LabelScale = 0.8;
            Style2.IconColor = Color.White;
            Style2.IconOpacity = 100;
            Style2.IconScale = 0.8;
            Style2.FillColor = Color.DarkRed;
            //wp.StyleOptions.IconPath = "http://maps.google.com/mapfiles/kml/shapes/donut.png";
            Style2.IconPath = "";
            Style2.FillOpacity = 50;

            // Style 3 : Blue Airspace/Corridor
            Style Style3 = new Style();
            Style3.Name = "Blue Airspace/Corridor";
            Style3.LineColor = Color.Blue;
            Style3.LineWidth = 1.0;
            Style3.Extrude = true;
            Style3.LabelColor = Color.White;
            Style3.LabelOpacity = 100;
            Style3.LabelScale = 0.8;
            Style3.IconColor = Color.White;
            Style3.IconOpacity = 100;
            Style3.IconPath = "";
            Style3.IconScale = 0.8;
            Style3.FillColor = Color.DarkBlue;
            Style3.FillOpacity = 50;

            // Style 4 : Green Airspace/Corridor
            Style Style4 = new Style();
            Style4.Name = "Green Airspace/Corridor";
            Style4.LineColor = Color.Green;
            Style4.LineWidth = 1.0;
            Style4.Extrude = true;
            Style4.LabelColor = Color.White;
            Style4.LabelOpacity = 100;
            Style4.LabelScale = 0.8;
            Style4.IconColor = Color.White;
            Style4.IconOpacity = 100;
            Style4.IconPath = "";
            Style4.IconScale = 0.8;
            Style4.FillColor = Color.DarkGreen;
            Style4.FillOpacity = 50;

            // Style 5
            Style Style5 = new Style();
            Style5.Name = "ICAO Tower";
            Style5.LineColor = Color.Green;
            Style5.LineWidth = 1.0;
            Style5.Extrude = true;
            Style5.LabelColor = Color.Red;
            Style5.LabelOpacity = 100;
            Style5.LabelScale = 1.0;
            Style5.IconColor = Color.White;
            Style5.IconOpacity = 100;
            Style5.IconPath = Statics.ApplicationPath + @"\Icons\ICAO\ICAO-HIGH.png";
            Style5.IconScale = 1.0;            

            Styles.Add(Style1);
            Styles.Add(Style2);
            Styles.Add(Style3);
            Styles.Add(Style4);
            Styles.Add(Style5);
        }

        public bool Export(string filename)
        {
            // This function does all the work of saving a "KMLFile" object
            // into a Google Earth .KML file            
            XmlTextWriter OutputFile;

            // STEP 1: Create/Open File and error check
            try
            {
                OutputFile = new XmlTextWriter(filename, null);
            }
            catch
            {
                return false;
            }            

            // STEP 2: Write Header
            // 2a. Create a "description" string
            string desc = "Name: " + this.Name + "\n";
            desc += "Date: " + this.Date + "\n";
            desc += "Classification: " + this.Classification + "\n\n";
            desc += "Author: " + this.Author + "\n";
            desc += "Organization: " + this.Organization + "\n";
            desc += "Phone: " + this.Phone + "\n";
            desc += "E-mail: " + this.Email + "\n";
            desc += "Website: " + this.Website + "\n\n";
            desc += "Description: " + this.Description;

            // 2b. Output the header
            OutputFile.Formatting = Formatting.Indented;
            OutputFile.Indentation = 6;
            OutputFile.Namespaces = false;

            OutputFile.WriteStartDocument();
            OutputFile.WriteStartElement("kml");
            OutputFile.WriteStartElement("Document");

            OutputFile.WriteStartElement("name");
            OutputFile.WriteString(this.Name);
            OutputFile.WriteEndElement();

            OutputFile.WriteStartElement("Snippet");
            OutputFile.WriteString(this.Snippet);
            OutputFile.WriteEndElement();

            OutputFile.WriteStartElement("description");
            OutputFile.WriteString(desc);
            OutputFile.WriteEndElement();

            

            
            // STEP 3: WRITE STYLES
            // 3a: Write predefined styles
            // For objects that use "custom" styles, they will be written to the
            // file inline within the feature.
            foreach (Style s in Styles)
                s.Export(OutputFile);
            
            // STEP 4: WRITE DATA            
            foreach (GETObject obj in RootObject.Children)
                obj.Export(OutputFile);
            //RootObject.Export(OutputFile);
            
            // STEP 5: Closing Tags
            OutputFile.WriteEndElement(); // Document
            OutputFile.WriteEndElement(); // kml

            OutputFile.Close();

            return true;
        }



        #endregion






    }
}
