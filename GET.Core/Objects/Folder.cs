/********************************************************************************/
/* Class:   GET.Core.Objects.Folder                                             */
/*                                                                              */
/* A "folder" is a very basic GETObject that stores other objects.  It contains */
/* almost no functionality beyond what is coded in GETObject.  It has its own   */
/* data type mainly for clarity's sake in the code that uses it.  It does have  */
/* its own unique Export() function for generating a .KML folder.               */
/*                                                                              */
/* Public Properties:                                                           */
/*  List<GetObject> Children    Child objects which this object contains        */
/*  string Description          A description for the object                    */
/*  DateTime EndTime            The end time associated with this feature.      */
/*                              Only applies if UseTimeStamp is set TRUE.       */
/*  bool IncludeInExport        Toggles whether the object and its descendents  */
/*                              will be included in the .KML export             */
/*  string Name                 A name for the object                           */
/*  bool ShowVisible            Toggles whether the object is checked visible   */
/*                              when the .KML is loaded in GE                   */
/*  GETObject Parent            The parent object which owns this object        */
/*  string Snippet              In GE, a "snippet" is a short two-line          */
/*                              description that appears just beneath the name  */
/*                              of a feature.                                   */
/*  DateTime StartTime          The start time associated with this feature.    */
/*                              Only applies if UseTimeStamp is set TRUE.       */
/*  Style StyleOptions          The "Style" object defining how this object     */
/*                              will appear in GE                               */
/*                                                                              */
/* bool Export(XmlTextWriter OutputFile) - COMPLETE                             */
/*                                                                              */
/*      This function adds a new .KML Folder object to an existing .KML file.   */
/*      The single argument is an "XmlTextWriter" object, which is generated in */
/*      Project.CreateKML().  The function will then call the Export()          */
/*      function for all children, ensuring itself and all children get added   */
/*      to the .KML.                                                            */
/*                                                                              */
/* Folder GetCopy() - COMPLETE                                                  */
/*                                                                              */
/*      Most GETObject data types have a GetCopy() function, which generates    */
/*      and returns a deep copy of the object.                                  */
/*                                                                              */
/********************************************************************************/


using System;
using System.Collections.Generic;
using System.Text;

namespace GET.Core.Objects
{
    [Serializable]
    public class Folder : GETObject
    {
        public Folder()
        {
            Name = "New Folder";
        }

        public override bool Export(System.Xml.XmlTextWriter OutputFile)
        {         
            // Error check.  Only export if the OutputFile is valid.  Return
            // false if the file is invalid because an error exists.
            if (OutputFile == null)
                return false;
            // Check to see if the user wants this object included in the export.
            // If he doesn't, we still return "True" because the function is
            // working properly.
            if (this.IncludeInExport == false)
                return true;

            // Write basic data for the folder
            OutputFile.WriteStartElement("Folder");

            OutputFile.WriteStartElement("name");
            OutputFile.WriteString(this.Name);
            OutputFile.WriteEndElement(); // name

            OutputFile.WriteStartElement("Snippet");
            OutputFile.WriteString(this.Snippet);
            OutputFile.WriteEndElement();

            OutputFile.WriteStartElement("description");
            OutputFile.WriteString(this.Description);
            OutputFile.WriteEndElement(); // description

            OutputFile.WriteStartElement("visibility");
            if (this.ShowVisible == true)
                OutputFile.WriteString("1");
            else
                OutputFile.WriteString("0");
            OutputFile.WriteEndElement();

            // Timestamp
            if (this.UseTimeStamp == true)
            {
                OutputFile.WriteStartElement("TimeSpan");
                OutputFile.WriteStartElement("begin");
                OutputFile.WriteString(this.StartTimeKMLFormat);
                OutputFile.WriteEndElement(); // begin
                OutputFile.WriteStartElement("end");
                OutputFile.WriteString(this.EndTimeKMLFormat);
                OutputFile.WriteEndElement(); // end
                OutputFile.WriteEndElement(); // TimeSpan
            }

            foreach (GETObject obj in this.Children)
                obj.Export(OutputFile);

            OutputFile.WriteEndElement(); // Folder



            return true;
        }

        public new Folder GetCopy()
        {
            Folder newFolder = new Folder();
            newFolder.Description = this.Description;
            newFolder.IncludeInExport = this.IncludeInExport;
            newFolder.Name = this.Name;
            newFolder.ShowVisible = this.ShowVisible;
            newFolder.Snippet = this.Snippet;
            newFolder.UseTimeStamp = this.UseTimeStamp;
            newFolder.StartTime = this.StartTime;
            newFolder.EndTime = this.EndTime;  
            newFolder.StyleOptions = this.StyleOptions;

            foreach (GETObject obj in this.Children)
            {
                if (obj is Waypoint)
                {
                    Waypoint wp = ((Waypoint)obj).GetCopy();
                    newFolder.AddChild(wp);
                }
                else if (obj is Route)
                {
                    Route rte = ((Route)obj).GetCopy();
                    newFolder.AddChild(rte);
                }
                else if (obj is AirspaceRegion)
                {
                    AirspaceRegion ar = ((AirspaceRegion)obj).GetCopy();
                    newFolder.AddChild(ar);
                }
                else if(obj is Folder)
                {
                    Folder fold = ((Folder)obj).GetCopy();
                    newFolder.AddChild(fold);
                }
                else if (obj is Threat)
                {
                    Threat t = ((Threat)obj).GetCopy();
                    newFolder.AddChild(t);
                }
            }
            return newFolder;
        }
    }
}
