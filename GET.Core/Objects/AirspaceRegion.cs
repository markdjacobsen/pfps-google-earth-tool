/********************************************************************************/
/* Class:   GET.Core.Objects.AirspaceRegion()                                   */
/*                                                                              */
/* This class is derived from GETObject() and contains a 3D volume of airspace. */
/* It currently supports five types of airspace: (1) circle (2) ellipse         */
/* (3) wedge (4) rectangle and (5) polygon.  Each stores slightly different     */
/* data and uses different functions to export to KML.                          */
/*                                                                              */
/* Public Properties:                                                           */
/*  int Altitude                Altitude/Elevation associated with the route.   */
/*                              Whether it's MSL or AGL will depend on          */
/*                              AltitudeType.                                   */
/*  AltType AltitudeType        Specifies whether the route is clamped to the   */
/*                              ground or whether it's an MSL or AGL altitude   */
/*  List<GetObject> Children    Child objects which this object contains        */
/*  string Description          A description for the object                    */
/*  DateTime EndTime            The end time associated with this feature.      */
/*                              Only applies if UseTimeStamp is set TRUE.       */
/*  bool GenerateFullPath       Toggles whether a continuous path line will be  */
/*                              generated in the .KML file                      */
/*  bool GeneratePlacemarks     Toggles whether placemarks will be generated at */
/*                              each turnpoint                                  */
/*  bool IncludeInExport        Toggles whether the object and its descendents  */
/*                              will be included in the .KML export             */
/*  string Name                 A name for the object                           */
/*  bool ShowVisible            Toggles whether the object is checked visible   */
/*                              when the .KML is loaded in GE                   */
/*  GETObject Parent            The parent object which owns this object        */
/*  string Snippet              In GE, a "snippet" is a short two-line          */
/*                              description that appears just beneath the name  */
/*                              of a feature.                                   */
/*  DateTime StartTime          The start time associated with this feature.    */
/*                              Only applies if UseTimeStamp is set TRUE.       */
/*  Style StyleOptions          The "Style" object defining how this object     */
/*                              will appear in GE                               */
/*                                                                              */
/* AirspaceRegion GetCopy() - COMPLETE                                          */
/*                                                                              */
/*      Most GETObject data types have a GetCopy() function, which generates    */
/*      and returns a deep copy of the object.                                  */
/*                                                                              */
/* bool Export(XmlTextWriter OutputFile) - IN PROGRESS                          */
/*                                                                              */
/*      Generates .KML output to the XmlTextWriter based on the data and        */
/*      the export settings.  This function will generally be called from       */
/*      Project.CreateKML(), which creates the .KML file and the XmlTextWriter. */
/*                                                                              */
/********************************************************************************/


using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace GET.Core.Objects
{
    [Serializable]
    public class AirspaceRegion : GETObject
    {
        #region Private Member Data                     
        
        private AirspaceType airspaceRegionType = AirspaceType.Circle;
        private int altitudeCeiling = 1500;
        private int altitudeFloor = 0;        
        private AltType altitudeType = AltType.FixedAGL;
        private bool generatePlacemark = true;
        private LatLon centerPoint = new LatLon();        

        // Circle and wedge properties
        private decimal innerRadius = 0;
        private decimal outerRadius = 5;        
        private int resolution = 36;
        private double startRadial = 0;
        private double stopRadial = 360;        

        // Ellipse properties
        private double angle = 0;
        private decimal horizontalRadius = 5;
        private decimal verticalRadius = 5;        

        // Rectangle properties
        private double rectWidth = 5;
        private double rectHeight = 5;        
        
        // Polygon properties                
        private List<LatLon> polygonPoints = new List<LatLon>();        

        // Export data
        private List<LatLon> innerPoints;
        private List<LatLon> outerPoints;
        #endregion

        #region Public Properties
        
        public AirspaceType AirspaceRegionType
        {
            get { return airspaceRegionType; }
            set { airspaceRegionType = value; }
        }

        public int AltitudeCeiling
        {
            get { return altitudeCeiling; }
            set { altitudeCeiling = value; }
        }
        public int AltitudeFloor
        {
            get { return altitudeFloor; }
            set { altitudeFloor = value; }
        }
        public AltType AltitudeType
        {
            get { return altitudeType; }
            set { altitudeType = value; }
        }
        public double Angle
        {
            get { return angle; }
            set { angle = value; }
        }
        public LatLon CenterPoint
        {
            get { return centerPoint; }
            set { centerPoint = value; }
        }
        public bool GeneratePlacemark
        {
            get { return generatePlacemark; }
            set { generatePlacemark = value; }
        }
        public decimal HorizontalRadius
        {
            get { return horizontalRadius; }
            set { horizontalRadius = value; }
        }
        public decimal InnerRadius
        {
            get { return innerRadius; }
            set { innerRadius = value; }
        }
        public decimal OuterRadius
        {
            get { return outerRadius; }
            set { outerRadius = value; }
        }
        public double RectHeight
        {
            get { return rectHeight; }
            set { rectHeight = value; }
        }
        public double RectWidth
        {
            get { return rectWidth; }
            set { rectWidth = value; }
        }
        public int Resolution
        {
            get { return resolution; }
            set { resolution = value; }
        }        
        public double StartRadial
        {
            get { return startRadial; }
            set { startRadial = value; }
        }
        public double StopRadial
        {
            get { return stopRadial; }
            set { stopRadial = value; }
        }                                

        public List<LatLon> PolygonPoints
        {
            get { return polygonPoints; }
            set { polygonPoints = value; }
        }
        public decimal VerticalRadius
        {
            get { return verticalRadius; }
            set { verticalRadius = value; }
        }
        
        #endregion

        #region Methods
        public override bool Export(XmlTextWriter OutputFile)
        {
            // This is a switching function.  For clarity's sake, the different
            // types of airspace objects have different export functions.  Note
            // that Circle and Wedge objects both use the ExportCircle() function,
            // because the code is essentially identical.
            
            // Error check... make sure we have a valid file open
            if (OutputFile == null)
                return false;

            // Don't write it, if it's not supposed to be included in the export
            if(this.IncludeInExport == false)
                return true;

            // Header information... this is the same for every type of airspace
            // region.  This function creates the Placemark and basic properties.
            ExportHeader(OutputFile);

            // Here are the guts of the number-crunching.  Depending on the type
            // of airspace, we call a separate "Generate" function, which builds
            // a list of "outerPoints" and "innerPoints".
            if(this.AirspaceRegionType == AirspaceType.Polygon)            
                GeneratePolygon();                
            else if(this.AirspaceRegionType == AirspaceType.Circle)
                GenerateCircle();
            else if(this.AirspaceRegionType == AirspaceType.Ellipse)
                GenerateEllipse();
            else if(this.AirspaceRegionType == AirspaceType.Rectangle)
                GenerateRectangle();
            else if(this.AirspaceRegionType == AirspaceType.Wedge)            
                GenerateWedge();                         
   
            // This class uses two very different methods to export polygons,
            // depending on whether or not the airspace region reaches down to
            // the surface.  If it does, we can create a single polygon and use
            // GE's "extrude" feature to extrude it down to the earth.  If the
            // airspace has a floor, however, we need to generate dozens of
            // polygons stretching from the floor to the ceiling.
            // These two methods are broken into separate functions.
            if (this.AltitudeFloor == 0 || this.AltitudeType == AltType.ClampToGround)
                ExportPolygonsExtrudeMethod(OutputFile);
            else             
                ExportPolygonsNormalMethod(OutputFile);
            //   ExportPolygonsExtrudeMethod(OutputFile);

           
            // Footer information... this mainly has closing tags for the Placemark
            ExportFooter(OutputFile);

            // Now create a placemark
            if(this.GeneratePlacemark == true)
                ExportPlacemark(OutputFile);

            return true;
        }

        private void ExportHeader(XmlTextWriter OutputFile)
        {
            OutputFile.WriteStartElement("Placemark");
            OutputFile.WriteStartElement("name");
            OutputFile.WriteString(this.Name);
            OutputFile.WriteEndElement(); // name

            OutputFile.WriteStartElement("Snippet");
            OutputFile.WriteString(this.Snippet);
            OutputFile.WriteEndElement(); // snippet

            OutputFile.WriteStartElement("description");
            OutputFile.WriteString(this.Name);
            OutputFile.WriteEndElement(); // description

            OutputFile.WriteStartElement("visibility");
            if (this.ShowVisible == true)
                OutputFile.WriteString("1");
            else
                OutputFile.WriteString("0");
            OutputFile.WriteEndElement();

            // Timestamp
            if (this.UseTimeStamp == true)
            {
                OutputFile.WriteStartElement("TimeSpan");
                OutputFile.WriteStartElement("begin");
                OutputFile.WriteString(this.StartTimeKMLFormat);
                OutputFile.WriteEndElement(); // begin
                OutputFile.WriteStartElement("end");
                OutputFile.WriteString(this.EndTimeKMLFormat);
                OutputFile.WriteEndElement(); // end
                OutputFile.WriteEndElement(); // TimeSpan
            }

             // Now export the style.  This can take two different forms:
            // (1) Pre-defined Style is being used.  We need to reference the
            //     StyleID for the saved style.
            // (2) Custom style sheet name.  Export the style inline.
            if (this.StyleOptions.Name == "<Custom>")
            {
                // We have a custom style.  Export the style as inline.
                this.StyleOptions.Export(OutputFile);
            }
            else
            {
                // We have a reusable Style specified.  Export a reference
                // to the saved style.  See GET.Core.Project.Export() for
                // the code that exports the style.                
                OutputFile.WriteStartElement("styleUrl");
                OutputFile.WriteString("#" + this.StyleOptions.Name);
                OutputFile.WriteEndElement(); // styleUrl
            }            
        }

        private void ExportFooter(XmlTextWriter OutputFile)
        {            
            OutputFile.WriteEndElement(); // Placemark
        }

        private void ExportPolygonsExtrudeMethod(XmlTextWriter OutputFile)
        {
            ExportPolygonsExtrudeMethod(OutputFile, -1);
        }
        
        private void ExportPolygonsExtrudeMethod(XmlTextWriter OutputFile, int AltOverride)
        {             
            // This function can be called two different ways:
            // (1) It can be called from this.Export() if the object does not use a floor altitude.
            // (2) It can be called from this.ExportPolygonsNormalMethod() to create a ceiling polygon or a
            //     floor polygon at a fixed altitude.
            // By default, AltOverride is set to -1, which specifies that we should draw an extruded polygon
            // at this.CeilingAltitude.  If AltOverride is set to other than -1, a flat disc (not extruded)
            // is drawn at the specified override altitude.

            OutputFile.WriteStartElement("Polygon");
            OutputFile.WriteAttributeString("id", this.Name);
            
            OutputFile.WriteStartElement("extrude");
            if (AltOverride == -1)
                OutputFile.WriteString("1");
            else
                OutputFile.WriteString("0");
            OutputFile.WriteEndElement(); // extrude

            OutputFile.WriteStartElement("altitudeMode");
            if (this.AltitudeType == AltType.FixedMSL)
                OutputFile.WriteString("absolute");
            else if(this.AltitudeType == AltType.ClampToGround)
                OutputFile.WriteString("clampToGround");
            else
                OutputFile.WriteString("relativeToGround");
            OutputFile.WriteEndElement(); // altitudeMode

            OutputFile.WriteStartElement("tessellate");
            OutputFile.WriteString("1");
            OutputFile.WriteEndElement(); // tessellate
            
            // Write the outer portion
            OutputFile.WriteStartElement("outerBoundaryIs");
            OutputFile.WriteStartElement("LinearRing");
            OutputFile.WriteStartElement("coordinates");
            
            // This code block builds the lat/lon strings
            string s = "";

            foreach (LatLon latlon in this.outerPoints)
            {
                double AltMeters = 0;
                if (AltOverride == -1)
                    AltMeters = Convert.ToDouble(this.AltitudeCeiling) / 3.2808399;
                else
                    AltMeters = Convert.ToDouble(AltOverride) / 3.2808399;
                string exportAltitude = AltMeters.ToString();                
                s += latlon.LongitudeDouble + "," + latlon.LatitudeDouble + "," + exportAltitude + " ";
            }            
            OutputFile.WriteString(s);
            OutputFile.WriteEndElement(); // coordinates
            OutputFile.WriteEndElement(); // LinearRing
            OutputFile.WriteEndElement(); // outerBoundaryIs      
     
            // For circles with an inner radius, write the inner portion
            if (this.AirspaceRegionType == AirspaceType.Circle && this.InnerRadius > 0)
            {
                OutputFile.WriteStartElement("innerBoundaryIs");
                OutputFile.WriteStartElement("LinearRing");
                OutputFile.WriteStartElement("coordinates");

                // This code block builds the lat/lon strings
                s = "";

                foreach (LatLon latlon in this.innerPoints)
                {
                    double AltMeters = Convert.ToDouble(this.AltitudeCeiling) / 3.2808399;
                    string exportAltitude = AltMeters.ToString();
                    s += latlon.LongitudeDouble + "," + latlon.LatitudeDouble + "," + exportAltitude + " ";
                }
                OutputFile.WriteString(s);
                OutputFile.WriteEndElement(); // coordinates
                OutputFile.WriteEndElement(); // LinearRing
                OutputFile.WriteEndElement(); // innerBoundaryIs                      
            }
            OutputFile.WriteEndElement(); // Polygon
        }   

        private void ExportPolygonsNormalMethod(XmlTextWriter OutputFile)
        {
            // Necessary for grouping polygons
            OutputFile.WriteStartElement("MultiGeometry");                           

            // Calculate the altitudes that will be used for each polygon
            double AltFloorMeters = Convert.ToDouble(this.AltitudeFloor) / 3.2808399;
            double AltCeilingMeters = Convert.ToDouble(this.AltitudeCeiling) / 3.2808399;
            string strAltFloorMeters = AltFloorMeters.ToString();
            string strAltCeilingMeters = AltCeilingMeters.ToString();

            

            // This function generates a series of polygons that stand vertically
            // on end, in order to create the sidewalls of an airspace region.
            for (int i = 0; i < outerPoints.Count - 1; i++)
            {
                OutputFile.WriteStartElement("Polygon");
                OutputFile.WriteAttributeString("id", this.Name + "." + i);

                OutputFile.WriteStartElement("extrude");
                OutputFile.WriteString("0");
                OutputFile.WriteEndElement(); // extrude

                OutputFile.WriteStartElement("altitudeMode");
                if (this.AltitudeType == AltType.FixedMSL)
                    OutputFile.WriteString("absolute");
                else
                    OutputFile.WriteString("relativeToGround");
                OutputFile.WriteEndElement(); // altitudeMode

                OutputFile.WriteStartElement("tessellate");
                OutputFile.WriteString("1");
                OutputFile.WriteEndElement(); // tessellate

                // Write the outer portion
                OutputFile.WriteStartElement("outerBoundaryIs");
                OutputFile.WriteStartElement("LinearRing");
                OutputFile.WriteStartElement("coordinates");

                // This code block builds the lat/lon strings
                string s = outerPoints[i].LongitudeDouble + "," + outerPoints[i].LatitudeDouble + "," + strAltFloorMeters + " ";
                s += outerPoints[i + 1].LongitudeDouble + "," + outerPoints[i + 1].LatitudeDouble + "," + strAltFloorMeters + " ";
                s += outerPoints[i + 1].LongitudeDouble + "," + outerPoints[i + 1].LatitudeDouble + "," + strAltCeilingMeters + " ";
                s += outerPoints[i].LongitudeDouble + "," + outerPoints[i].LatitudeDouble + "," + strAltCeilingMeters + " ";
                OutputFile.WriteString(s);
                OutputFile.WriteEndElement(); // coordinates
                OutputFile.WriteEndElement(); // LinearRing
                OutputFile.WriteEndElement(); // outerBoundaryIs       
                OutputFile.WriteEndElement(); // Polygon                
            }

            // Now draw the floor disk
            ExportPolygonsExtrudeMethod(OutputFile, this.AltitudeFloor);            

            // Now draw the ceiling disk
            ExportPolygonsExtrudeMethod(OutputFile, this.AltitudeCeiling);

            OutputFile.WriteEndElement(); // MultiGeometry
        }


        private void ExportPlacemark(XmlTextWriter OutputFile)
        {
            // This code generates a placemark at the center of the airspace region            
            Waypoint Centroid = new Waypoint();
            
            // Give the waypoint the same properties as the owning region
            Centroid.Name = this.Name;
            Centroid.Snippet = this.Snippet;
            Centroid.Description = this.Description;
            Centroid.ShowVisible = this.ShowVisible;
            Centroid.UseTimeStamp = this.UseTimeStamp;
            Centroid.StartTime = this.StartTime;
            Centroid.EndTime = this.EndTime;
            
            Centroid.StyleOptions = this.StyleOptions;
            Centroid.AltitudeType = this.altitudeType;
            Centroid.Altitude = (this.AltitudeFloor + this.altitudeCeiling)/2;
            if (this.AirspaceRegionType == AirspaceType.Circle || this.AirspaceRegionType == AirspaceType.Ellipse ||
                this.AirspaceRegionType == AirspaceType.Rectangle)
                Centroid.Coordinates = this.CenterPoint;
            else
            {
                double LatSum = 0;
                double LonSum = 0;
                foreach (LatLon latlon in this.outerPoints)
                {
                    LatSum += latlon.LatitudeDouble;
                    LonSum += latlon.LongitudeDouble;
                }
                LatSum /= this.outerPoints.Count;
                LonSum /= this.outerPoints.Count;
                Centroid.Coordinates.Assign(LatSum, LonSum);
            }
            Centroid.Export(OutputFile);
        }        

        public new AirspaceRegion GetCopy()
        {
            AirspaceRegion newRegion = new AirspaceRegion();

            newRegion.Description = this.Description;
            newRegion.IncludeInExport = this.IncludeInExport;
            newRegion.Name = this.Name;
            newRegion.ShowVisible = this.ShowVisible;
            newRegion.Snippet = this.Snippet;
            newRegion.UseTimeStamp = this.UseTimeStamp;
            newRegion.StyleOptions = this.StyleOptions;
            newRegion.StartTime = this.StartTime;
            newRegion.EndTime = this.EndTime;

            newRegion.AirspaceRegionType = this.AirspaceRegionType;
            newRegion.AltitudeCeiling = this.AltitudeCeiling;
            newRegion.AltitudeFloor = this.AltitudeFloor;
            newRegion.AltitudeType = this.AltitudeType;
            newRegion.Angle = this.Angle;
            newRegion.CenterPoint = this.CenterPoint.GetCopy();
            newRegion.GeneratePlacemark = this.GeneratePlacemark;
            newRegion.HorizontalRadius = this.HorizontalRadius;
            newRegion.InnerRadius = this.InnerRadius;
            newRegion.OuterRadius = this.OuterRadius;
            newRegion.RectHeight = this.RectHeight;
            newRegion.RectWidth = this.RectWidth;
            newRegion.Resolution = this.Resolution;
            newRegion.StartRadial = this.StartRadial;
            newRegion.StopRadial = this.StopRadial;
            newRegion.VerticalRadius = this.VerticalRadius;            
            
             foreach (GETObject obj in this.Children)
            {
                if (obj is Waypoint)
                {
                    Waypoint wp = ((Waypoint)obj).GetCopy();
                    newRegion.AddChild(wp);
                }
                else if (obj is Route)
                {
                    Route rte = ((Route)obj).GetCopy();
                    newRegion.AddChild(rte);
                }
                else if (obj is AirspaceRegion)
                {
                    AirspaceRegion ar = ((AirspaceRegion)obj).GetCopy();
                    newRegion.AddChild(ar);
                }
                else if(obj is Folder)
                {
                    Folder fold = ((Folder)obj).GetCopy();
                    newRegion.AddChild(fold);
                }
                else if (obj is Threat)
                {
                    Threat t = ((Threat)obj).GetCopy();
                    newRegion.AddChild(t);
                }
            }

            return newRegion;
        }

        private void GenerateCircle()
        {
            outerPoints = new List<LatLon>();
            innerPoints = new List<LatLon>();

            // "resolution" will determine how many points are drawn.  The
            // radial spacing is calculated from this number
            double radialSpacing = 360/this.resolution;

            // Now we create the LatLons based on these radials
            // We simultaneous create "outerPoints" and "innerPoints"
            double radialIndex = 0;
            for (int i = 0; i < resolution; i++)
            {
                LatLon OuterPt = this.CenterPoint.RadialDME(radialIndex, (double)outerRadius);                
                outerPoints.Add(OuterPt);

                LatLon InnerPt = this.CenterPoint.RadialDME(radialIndex, (double)innerRadius);
                innerPoints.Add(InnerPt);
                radialIndex += radialSpacing;
            }          
            // this ensures we seal the polygon
            outerPoints.Add(outerPoints[0]);
            innerPoints.Add(innerPoints[0]);
        }

        private void GenerateEllipse()
        {
            outerPoints = new List<LatLon>();
            innerPoints = new List<LatLon>();
            // ZeroReference is (0,0) and will initially be used as the centerpoint of the ellipse.
            // We will move the whole ellipse once the points are generated
            LatLon ZeroReference = new LatLon();

            // "resolution" will determine how many points are drawn.  The
            // radial spacing is calculated from this number
            double radialSpacing = 360 / this.resolution;

            // Start by building a circle
            double radialIndex = 0;
            for (int i = 0; i < resolution; i++)
            {
                LatLon OuterPt = CenterPoint.RadialDME(radialIndex, (double)HorizontalRadius);
                outerPoints.Add(OuterPt);
                radialIndex += radialSpacing;
            }

            // this ensures we seal the polygon
            outerPoints.Add(outerPoints[0].GetCopy());

            // Now we stretch the ellipse.  We do this by creating a "multiplier" that is
            // the ratio of the vertical radius to the horizontal radius.  We will multiply
            // each (Latitude - CenterPoint.Latitude) by this multiplier to get the 
            // corrected point.
            double multiplier = (double)VerticalRadius / (double)HorizontalRadius;
                        
            foreach (LatLon coord in outerPoints)
            {
                // this ensures we seal the polygon
                double LatitudeCorrection = (coord.LatitudeDouble - CenterPoint.LatitudeDouble) * multiplier;
                coord.Assign(LatitudeCorrection + CenterPoint.LatitudeDouble, coord.LongitudeDouble);               
            }

            // TODO: add rotation code
            RotateOuterPoints();           
        }

        private void GeneratePolygon()
        {
            outerPoints = new List<LatLon>();

            // Polygons are easy because the points are already stored
            // in the object.  Here we just copy them into "outerPoints"
            foreach(LatLon coord in this.polygonPoints)
                outerPoints.Add(coord);

            // We ensure the loop is closed by repeating the first point
            outerPoints.Add(outerPoints[0]);
        }
        
        private void GenerateRectangle()
        {            
            double Left = CenterPoint.RadialDME(270, (double)RectWidth / 2).LongitudeDouble;
            double Right = CenterPoint.RadialDME(90, (double)RectWidth / 2).LongitudeDouble;
            double Top = CenterPoint.RadialDME(0, (double)RectHeight / 2).LatitudeDouble;
            double Bottom = CenterPoint.RadialDME(180, (double)RectHeight / 2).LatitudeDouble;

            outerPoints = new List<LatLon>();
            outerPoints.Add(new LatLon(Top,Left));
            outerPoints.Add(new LatLon(Top,Right));
            outerPoints.Add(new LatLon(Bottom, Right));
            outerPoints.Add(new LatLon(Bottom, Left));
            outerPoints.Add(new LatLon(Top, Left));
            
            RotateOuterPoints();          
        }

        private void GenerateWedge()
        {
            outerPoints = new List<LatLon>();
            innerPoints = new List<LatLon>();

            // Figure out how many radials will have points drawn.  We essentially
            // base this on the resolution for a 360-degree circle, corrected for
            // how big the arc actualy is.
            int numRadials = (int)((stopRadial - startRadial)/360 * resolution);
            double radialSpacing = (stopRadial - startRadial)/numRadials;

            // Now we create the LatLons based on these radials
            // We're going to store the entire polygon in "outerPoints"
            double radialIndex = startRadial;
            for (int i = 0; i < numRadials; i++)
            {
                LatLon NewPt = this.CenterPoint.RadialDME(radialIndex, (double)outerRadius);
                outerPoints.Add(NewPt);
                radialIndex += radialSpacing;
            }          
            // this ensures we seal the polygon
            outerPoints.Add(this.CenterPoint.RadialDME(stopRadial, (double)outerRadius));

            // Now loop around backwards to add the remainder
            radialIndex = stopRadial;
            for (int i = 0; i < numRadials; i++)
            {
                LatLon NewPt = this.CenterPoint.RadialDME(radialIndex, (double)innerRadius);
                outerPoints.Add(NewPt);
                radialIndex -= radialSpacing;
            }
            outerPoints.Add(this.CenterPoint.RadialDME(startRadial, (double)innerRadius));            
            // Close the loop
            outerPoints.Add(outerPoints[0]);
        }

        private void RotateOuterPoints()
        {
            foreach (LatLon coord in outerPoints)
            {
                double range = CenterPoint.GetRangeTo(coord);
                double bearing = CenterPoint.GetBearingTo(coord);
                LatLon newCoord = CenterPoint.RadialDME(bearing + angle, range);
                coord.Assign(newCoord.LatitudeDouble, newCoord.LongitudeDouble);
            }
        }
        #endregion
    }
}
