using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using GET.Core;
using GET.Core.PFPS.Internal;
using GET.Core.Objects;

namespace GET.Core.PFPS
{
    public class LocalPointFile
    {
        #region Private Member Data
        private List<Folder> groupFolders = new List<Folder>();
        private List<string> groupNames = new List<string>();
        private Folder objectFolder;
        private List<LocalPointObject> pfpsLocalPoints = new List<LocalPointObject>();
        private string title;
        #endregion

        #region Public Properties
        public Folder ObjectFolder
        {
            get { return objectFolder; }
            set { objectFolder = value; }
        }
        #endregion

        #region Methods
        public bool Load(string filename)
        {
            FileInfo dwfileinfo = new FileInfo(filename);
            string[] dwfileparts = dwfileinfo.Name.Split(new char[] { '.' });
            title = dwfileparts[0];

            // Open the FalconView Drawing File (stored as an Access DB)
            OleDbConnection connection;
            try
            {
                connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename);
                connection.Open();
            }
            catch
            {
                return false;
            }

            // Load the group names
            OleDbCommand groupCommand = connection.CreateCommand();
            groupCommand.CommandText = "SELECT Group_Name FROM Group_Names";
            OleDbDataReader groupReader = groupCommand.ExecuteReader();
            while(groupReader.Read())
            {
                groupNames.Add((string)groupReader["Group_Name"]);
            }
            groupReader.Close();


            // Load the points
            OleDbCommand pointCommand = connection.CreateCommand();

            pointCommand.CommandText = "SELECT ID, Description, Latitude, Longitude, Elevation, Elevation_Source, Pt_Quality,Area, Country_Code, Dtd_ID, Horz_Accuracy, Vert_Accuracy, Link_Name, Icon_Name, Comment, Group_Name FROM Points";
            OleDbDataReader pointReader = pointCommand.ExecuteReader();

            while (pointReader.Read())
            {
                ReadObject(pointReader);
            }
            pointReader.Close();
            return true;
        }

        private void ReadObject(OleDbDataReader reader)
        {
            LocalPointObject point = new LocalPointObject();
            point.Id = (string)reader["ID"];
            if(!(reader["Description"] is DBNull))
                point.Description = (string)reader["Description"];
            point.Latitude = (double)reader["Latitude"];
            point.Longitude = (double)reader["Longitude"];
            if(!(reader["Elevation"] is DBNull))
                point.Elevation = System.Convert.ToInt32((short)reader["Elevation"]);
            if(!(reader["Elevation_Source"] is DBNull))
                point.Elevation_source = (string)reader["Elevation_Source"];
            if(!(reader["Pt_Quality"] is DBNull))
                point.Pt_quality = System.Convert.ToDecimal((string)reader["Pt_Quality"]);
            if(!(reader["Area"] is DBNull))
                point.Area = (string)reader["Area"];
            if(!(reader["Country_Code"] is DBNull))
                point.Country_code = (string)reader["Country_Code"];
            if(!(reader["Dtd_ID"] is DBNull))
                point.Dtd_id = (string)reader["Dtd_ID"];
            if (!(reader["Horz_Accuracy"] is DBNull))
                point.Horz_accuracy = (float)reader["Horz_Accuracy"];
            if (!(reader["Vert_Accuracy"] is DBNull))
                point.Vert_accuracy = (float)reader["Vert_Accuracy"];
            if (!(reader["Link_Name"] is DBNull))
                point.Link_name = (string)reader["Link_Name"];
            if (!(reader["Icon_Name"] is DBNull))            
                point.Icon_name = (string)reader["Icon_Name"];                       
            if (!(reader["Comment"] is DBNull))
                point.Comment = (string)reader["Comment"];
            if (!(reader["Group_name"] is DBNull))
            point.Group_name = (string)reader["Group_Name"];

            pfpsLocalPoints.Add(point);
        }

        public Folder Convert()
        {
            // Create the parent folder
            ObjectFolder = new Folder();
            ObjectFolder.Name = title;
            ObjectFolder.Snippet = "FalconView Local Point File";

            // Create the group subfolders
            foreach (string groupName in groupNames)
            {
                Folder subFolder = new Folder();
                subFolder.Name = groupName;
                ObjectFolder.AddChild(subFolder);
                this.groupFolders.Add(subFolder);
            }

            // Convert/load all the points
            foreach (LocalPointObject point in this.pfpsLocalPoints)
            {
                Waypoint wp = point.Convert();
                int groupIndex = GetGroupIndex(point.Group_name);
                this.groupFolders[groupIndex].AddChild(wp);
            }

            return ObjectFolder;
        }

        private int GetGroupIndex(string groupName)
        {
            for (int i = 0; i < groupNames.Count; i++)
            {
                if (groupName == groupNames[i])
                    return i;
            }
            return 0;
        }
        #endregion
    }
}
