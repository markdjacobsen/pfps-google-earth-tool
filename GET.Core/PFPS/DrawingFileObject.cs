/********************************************************************************/
/* Class:   GET.Core.PFPS.DrawingFileObject()                                   */
/*                                                                              */
/* Each instance of this class corresponds to a single object in a FalconView   */
/* drawing file.  Object data is usually populated from DrawingFile.Load().     */
/* Lines of Drawing File data are parsed using the DrawingFileObject.AddLine()  */
/* function.                                                                    */
/*                                                                              */
/* Supported Object Types:                                                      */
/*   - Oval                                                                     */
/*   - Line                                                                     */
/*   - Rectangle                                                                */
/*                                                                              */
/* Public Properties:                                                           */
/*                                                                              */
/* Methods:                                                                     */
/*                                                                              */
/* void AddLine(int DataType, string Data)                                      */
/*  STATUS: IN PROGRESS                                                         */
/*                                                                              */
/*      This function parses a line of data from a Drawing File.  The function  */
/*      serves as a relay, based on the DataType.  It calls a different         */
/*      function for each type of DataType.  This is to keep the code clean.    */
/*                                                                              */
/********************************************************************************/


using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using GET.Core.Objects;

namespace GET.Core.PFPS.Internal
{
    public enum DrawingFileObjectType { Line, Ellipse, Circle, Rectangle, Text, Bullseye, Arrow, Polygon };
    public enum DrawingFillStyle { None, Horz, Vert, BDiag, FDiag, Cross, Diagcross, Solid, Shade };

    public class DrawingFileObject
    {
        #region Private Member Data
        // Basic Drawing File Data Types
        private DrawingFileObjectType objectType;
        private int id;        
        private string comment = "";
        private string text = "";
        private string tooltipText = "";
        private string helpText = "";
        private string label = "";
        private string textString = "";           

        // Style Properties
        private int fillStyle;        
        private Color fillColor;
        private Color labelColor;        
        private Color lineColor;
        private int lineWidth;        

        // Ellipse/Oval Properties
        private decimal horizontalRadius;
        private decimal verticalRadius;
        private decimal angle;
        private LatLon centerCoordinates = new LatLon();    
    
        // Rectangle Properties
        private double rectHeight;
        private double rectWidth;        

        // Polygon/Line Properties
        private List<LatLon> polygonPoints = new List<LatLon>();
        private bool isPolygon;
        
        private int altitudeCeiling = 1500;
        private int altitudeFloor = 0;        
        private AltType altitudeType = AltType.FixedAGL;                                                                            
        #endregion

        #region Public Properties

        public int AltitudeCeiling
        {
            get { return altitudeCeiling; }
            set { altitudeCeiling = value; }
        }
        public int AltitudeFloor
        {
            get { return altitudeFloor; }
            set { altitudeFloor = value; }
        }
        public AltType AltitudeType
        {
            get { return altitudeType; }
            set { altitudeType = value; }
        }
        public decimal Angle
        {
            get { return angle; }
            set { angle = value; }
        }
        public string Comment
        {
            get { return comment; }
            set { comment = value; }
        }
        public LatLon CenterCoordinates
        {
            get { return centerCoordinates; }
            set { centerCoordinates = value; }
        }
        public Color FillColor
        {
            get { return fillColor; }
            set { fillColor = value; }
        }
        public int FillStyle
        {
            get { return fillStyle; }
            set { fillStyle = value; }
        }
        public decimal HorizontalRadius
        {
            get { return horizontalRadius; }
            set { horizontalRadius = value; }
        }
        public string HelpText
        {
            get { return helpText; }
            set { helpText = value; }
        }
        public int ID
        {
            get { return id; }
            set { id = value; }
        }        
        public bool IsPolygon
        {
            get { return isPolygon; }
            set { isPolygon = value; }
        }
        public string Label
        {
            get { return label; }
            set { label = value; }
        }
        public Color LabelColor
        {
            get { return labelColor; }
            set { labelColor = value; }
        }        
        public Color LineColor
        {
            get { return lineColor; }
            set { lineColor = value; }
        }
        public int LineWidth
        {
            get { return lineWidth; }
            set { lineWidth = value; }
        }
        public DrawingFileObjectType ObjectType
        {
            get { return objectType; }
            set { objectType = value; }
        }       
        public List<LatLon> Points
        {
            get { return polygonPoints; }
            set { polygonPoints = value; }
        }
        public double RectHeight
        {
            get { return rectHeight; }
            set { rectHeight = value; }
        }
        public double RectWidth
        {
            get { return rectWidth; }
            set { rectWidth = value; }
        }
        public string Text
        {
            get { return text; }
            set { text = value; }
        }
        public string TextString
        {
            get { return textString; }
            set { textString = value; }
        }
        public string TooltipText
        {
            get { return tooltipText; }
            set { tooltipText = value; }
        }
        public decimal VerticalRadius
        {
            get { return verticalRadius; }
            set { verticalRadius = value; }
        }
        #endregion

        #region Methods
        public void AddLine(int DataType, string Data)
        {
            switch (DataType)
            {
                case 1:     // Overall information
                    ParseDataType1(Data);
                    break;
                case 3:
                case 4:
                    ParseDataType3(Data);
                    break;
                case 5:     // Centerpoint
                    ParseDataType5(Data);
                    break;
                case 6: // TextBox text
                    ParseDataType6(Data);
                    break;
                case 9:     // Tooltip Text
                    ParseDataType9(Data);
                    break;
                case 10:
                    ParseDataType10(Data);
                    break;
                case 11:    // Comment
                    ParseDataType11(Data);
                    break;
                case 12:    // Line/Fill Color
                    ParseDataType12(Data);
                    break;
                case 20:    // Label Color
                    ParseDataType20(Data);
                    break;
                case 24:    // Label
                    ParseDataType24(Data);
                    break;
            }
        }

        private void ParseDataType1(string text)
        {
            // DATA TYPE 1: DATA_TYPE_TYPE
            // (from SDK)
            // "This required record specifies the type of object and some
            // parameters associated with it."  See section 3.1 of ICD
            // for a breakdown of the data stored in the Data field
            //  - beginning with 01: Line
            //  - beginning with 03: Oval/Ellipse
            //  - beginning with 04: Text
            //  - beginning with 06: Bullseye
            //  - beginning with 07: Rectangle
            //  - beginning with 08: Axis of Advance           

            string objectTypeString = text.Substring(0, 2);
            switch (objectTypeString)
            {
                case "01":          // Line
                    this.ObjectType = DrawingFileObjectType.Line;
                    this.LineWidth = System.Convert.ToInt16(text.Substring(2, 2));                    
                    if (text[11] == 'Y')
                    {
                        this.IsPolygon = true;
                        this.FillStyle = System.Convert.ToInt16(text.Substring(7, 1));
                        this.ObjectType = DrawingFileObjectType.Polygon;
                    }
                    else
                        this.IsPolygon = false;
                    break;
                case "03":          // Ellipse
                    this.LineWidth = System.Convert.ToInt16(text.Substring(2, 2));
                    this.FillStyle = System.Convert.ToInt16(text.Substring(5, 1));
                    this.ObjectType = DrawingFileObjectType.Ellipse;
                    this.Angle = System.Convert.ToInt16(text.Substring(6, 3));
                    this.VerticalRadius = System.Convert.ToDecimal(text.Substring(9, 5));
                    this.HorizontalRadius = System.Convert.ToDecimal(text.Substring(14, 5));
                    this.VerticalRadius *= (decimal)0.539956803; // convert KM to NM
                    this.HorizontalRadius *= (decimal)0.539956803; // convert KM to NM
                    if (this.HorizontalRadius == this.VerticalRadius)
                        this.ObjectType = DrawingFileObjectType.Circle;
                    break;
                case "04":          // Text
                    this.ObjectType = DrawingFileObjectType.Text;
                    break;
                case "06":          // Bullseye
                    this.ObjectType = DrawingFileObjectType.Bullseye;
                    break;
                case "07":          // Rectangle
                    this.ObjectType = DrawingFileObjectType.Rectangle;
                    this.LineWidth = System.Convert.ToInt16(text.Substring(2, 2));
                    this.FillStyle = System.Convert.ToInt16(text.Substring(5, 1));
                    this.Angle = System.Convert.ToInt16(text.Substring(6, 3));                    
                    this.RectHeight = System.Convert.ToDouble(text.Substring(9,5));
                    this.RectWidth = System.Convert.ToDouble(text.Substring(14, 5));
                    this.RectHeight *= 0.539956803; // convert KM to NM
                    this.RectWidth *= 0.539956803; // convert KM to NM
                    break;
                case "08":          // Axis of advance
                    this.ObjectType = DrawingFileObjectType.Arrow;
                    break;
            }
            if (this.LineWidth == 0)
                this.LineWidth = 1;            
        }

        private void ParseDataType3(string text)
        {
            // Move-to or line-to point
            LatLon NewPoint = new LatLon();
            string latDir = text.Substring(0, 1);
            string loNDir = text.Substring(10, 1);
            string latNumber = text.Substring(1, 9);
            string lonNumber = text.Substring(11, 10);
            double latDouble = System.Convert.ToDouble(latNumber);
            double lonDouble = System.Convert.ToDouble(lonNumber);
            if (latDir == "S")
                latDouble = -latDouble;
            if (loNDir == "W")
                lonDouble = -lonDouble;

            NewPoint.Assign(latDouble, lonDouble);

            this.polygonPoints.Add(NewPoint);
        }

        private void ParseDataType5(string text)
        {
            // Center lat-lon
            string latDir = text.Substring(0, 1);
            string loNDir = text.Substring(10, 1);
            string latNumber = text.Substring(1, 9);
            string lonNumber = text.Substring(11, 10);
            double latDouble = System.Convert.ToDouble(latNumber);
            double lonDouble = System.Convert.ToDouble(lonNumber);
            if (latDir == "S")
                latDouble = -latDouble;
            if (loNDir == "W")
                lonDouble = -lonDouble;

            this.CenterCoordinates.Assign(latDouble, lonDouble);
        }

        private void ParseDataType6(string text)
        {
            string addString = text.Replace("\n", " ");
            addString = addString.Replace("\r", " ");
            this.TextString = this.TextString + addString;
        }

        private void ParseDataType9(string text)
        {
            // ToolTip Text
            string addString = text.Replace("\n", " ");
            addString = addString.Replace("\r", " ");
            this.TooltipText = this.TooltipText + addString;
        }

        private void ParseDataType10(string text)
        {
            // Help Text
            string addString = text.Replace("\n", " ");
            addString = addString.Replace("\r", " ");
            this.HelpText = this.HelpText + addString;            
        }

        private void ParseDataType11(string text)
        {
            // Comment
            string addString = text.Replace("\n", " ");
            addString = addString.Replace("\r", " ");
            this.Comment = this.Comment + addString;            
        }

        private void ParseDataType12(string text)
        {
            // Line/Fill Color
            LineColor = ParseColor(text.Substring(0, 3));
            FillColor = ParseColor(text.Substring(3, 3));
        }

        private void ParseDataType20(string text)
        {
            // Label Foreground/Background Color
            LabelColor = ParseColor(text.Substring(0, 3));
        }

        private void ParseDataType24(string text)
        {
            // Label Foreground/Background Color
            this.Label = this.Label + text + " ";
            this.Label = this.Label.Replace("\n", "");
            this.Label = this.Label.Replace("\r", "");
        }

        private Color ParseColor(string text)
        {
            // Color
            if (text == "000")
                return Color.Black;
            else if (text == "001")
                return Color.DarkRed;
            else if (text == "002")
                return Color.DarkGreen;
            else if (text == "003")
                return Color.Yellow;
            else if (text == "004")
                return Color.DarkBlue;
            else if (text == "005")
                return Color.DarkMagenta;
            else if (text == "006")
                return Color.DarkCyan;
            else if (text == "007")
                return Color.Gray;
            else if (text == "008")
                return Color.Green;
            else if (text == "009")
                return Color.SkyBlue;
            else if (text == "246")
                return Color.Tan;
            else if (text == "247")
                return Color.Gray;
            else if (text == "248")
                return Color.DarkGray;
            else if (text == "249")
                return Color.Red;
            else if (text == "250")
                return Color.Green;
            else if (text == "251")
                return Color.Yellow;
            else if (text == "252")
                return Color.Blue;
            else if (text == "253")
                return Color.Magenta;
            else if (text == "254")
                return Color.Cyan;
            else if (text == "255")
                return Color.White;
            else
                return Color.White;
        }

        public GETObject Convert()
        {
            // Converts this object into a GETObject (either Route, Waypoint or AirspaceRegion) and returns the result.
            // Axis of Advance is not ported
            // Ellipse, Rectangle, Line with IsPolygon = true -----> AirspaceRegion
            // Text Objects, Polygons -----> Waypoints
            // Line Objects w/IsPolygon = false ----> Route
            
            
            // We do not handle axis of advance (i.e. arrow) in this release
            if (this.ObjectType == DrawingFileObjectType.Arrow)
                return null;

            // Line objects that are not polygons become routes
            if (this.objectType == DrawingFileObjectType.Line && this.IsPolygon == false)
            {
                // Create the new object
                Route newRoute = new Route();

                // For line objects:
                // (1) The name (Google Earth label) will equal the FalconView Label
                // (2) The Google Earth snippet will equal the FalconView tooltiptext
                // (3) The Google Earth description will equal the FalconView Text and Comment
                newRoute.Name = this.Label;
                newRoute.Description = this.Label + "\n\n" + this.Text + "\n\n" + this.Comment;
                newRoute.Snippet = this.TooltipText;

                // Generate the altitude;
                int alt = 0;
                AltType altType = AltType.ClampToGround;
                GenerateAltitude(this.HelpText, ref altType, ref alt);
                newRoute.Altitude = alt;
                newRoute.AltitudeType = altType;

                // Generate the style
                newRoute.StyleOptions = new Style();
                newRoute.StyleOptions.FillColor = this.FillColor;
                newRoute.StyleOptions.LabelColor = this.LabelColor;
                newRoute.StyleOptions.LineColor = this.LineColor;
                newRoute.StyleOptions.LineWidth = this.LineWidth;
                newRoute.StyleOptions.LabelColor = Color.White;
                newRoute.StyleOptions.LabelScale = 0.8;
                newRoute.StyleOptions.IconPath = "";
                

                // Import points
                
                foreach (LatLon coord in this.polygonPoints)
                {
                    Waypoint point = new Waypoint();
                    point.Name = "";
                    point.Altitude = newRoute.Altitude;
                    point.AltitudeType = newRoute.AltitudeType;
                    point.Coordinates = coord.GetCopy();
                    point.StyleOptions = newRoute.StyleOptions;
                    newRoute.Points.Add(point);                    
                }

                return newRoute;
            }
            else if (this.ObjectType == DrawingFileObjectType.Text || this.ObjectType == DrawingFileObjectType.Bullseye)
            {
                Waypoint wp = new Waypoint();
                wp.Coordinates = this.CenterCoordinates.GetCopy();

                // Defaults... these are always inherited from the parent folder     
                if (this.ObjectType == DrawingFileObjectType.Bullseye)
                    wp.Name = this.TooltipText;
                else
                    wp.Name = this.TextString;
                wp.Description = this.Label + "\n\n" + this.Text + "\n\n" + this.Comment;
                wp.Snippet = this.TooltipText;
                wp.Altitude = 0;                
                wp.AltitudeType = AltType.ClampToGround;

                wp.StyleOptions = new Style();
                wp.StyleOptions.FillColor = this.FillColor;
                wp.StyleOptions.LabelColor = this.LabelColor;
                wp.StyleOptions.IconColor = this.LabelColor;
                wp.StyleOptions.LineColor = this.LineColor;
                wp.StyleOptions.LineWidth = this.LineWidth;
                wp.StyleOptions.LabelColor = Color.White;
                wp.StyleOptions.LabelScale = 0.8;
                if (this.ObjectType == DrawingFileObjectType.Bullseye)
                {
                    wp.StyleOptions.IconColor = this.LineColor;
                    wp.StyleOptions.IconPath = "http://maps.google.com/mapfiles/kml/shapes/target.png";
                }
                else
                {
                    // If not a bullseye, then it's a textbox.  We draw it in the item linecolor with no icon.
                    wp.StyleOptions.IconPath = "";
                    wp.StyleOptions.LabelColor = this.LineColor;
                    //wp.StyleOptions.IconPath = "http://maps.google.com/mapfiles/kml/shapes/info-i.png";                
                }

                return wp;
            }
            else
            {
                // Create the new object
                AirspaceRegion newRegion = new AirspaceRegion();

                // Set the object type
                if (this.objectType == DrawingFileObjectType.Circle)
                    newRegion.AirspaceRegionType = AirspaceType.Circle;
                else if (this.objectType == DrawingFileObjectType.Ellipse)
                    newRegion.AirspaceRegionType = AirspaceType.Ellipse;
                else if (this.objectType == DrawingFileObjectType.Rectangle)
                    newRegion.AirspaceRegionType = AirspaceType.Rectangle;
                else if (this.objectType == DrawingFileObjectType.Polygon)
                    newRegion.AirspaceRegionType = AirspaceType.Polygon;

                // Dimension info
                newRegion.Angle = (double)this.Angle;
                newRegion.CenterPoint = this.CenterCoordinates.GetCopy();
                newRegion.HorizontalRadius = this.HorizontalRadius;
                newRegion.VerticalRadius = this.VerticalRadius;
                newRegion.OuterRadius = this.VerticalRadius;
                newRegion.InnerRadius = 0;
                newRegion.StartRadial = 0;
                newRegion.StopRadial = 360;
                newRegion.RectHeight = this.RectHeight;
                newRegion.RectWidth = this.RectWidth;
                newRegion.Resolution = 36;

                // Defaults... these are always inherited from the parent folder     
                newRegion.Name = this.Label;
                newRegion.Description = this.Label + "\n\n" + this.Text + "\n\n" + this.Comment;
                
                // Copy the style for FalconView                
                newRegion.StyleOptions = new Style();
                newRegion.StyleOptions.FillColor = this.FillColor;
                newRegion.StyleOptions.LabelColor = this.LabelColor;
                newRegion.StyleOptions.LineColor = this.LineColor;
                newRegion.StyleOptions.LineWidth = this.LineWidth;
                newRegion.StyleOptions.LabelColor = Color.White;
                newRegion.StyleOptions.LabelScale = 0.8;
                newRegion.StyleOptions.IconColor = this.LineColor;
                newRegion.StyleOptions.IconScale = 0.5;
                newRegion.StyleOptions.IconPath = "http://maps.google.com/mapfiles/kml/shapes/donut.png";
                if (this.FillStyle == 0)
                    newRegion.StyleOptions.FillOpacity = 0;
                else
                    newRegion.StyleOptions.FillOpacity = 50;
                
                // Create points if this is a polygon
                if (this.ObjectType == DrawingFileObjectType.Polygon)
                {
                    foreach (LatLon coord in this.polygonPoints)
                        newRegion.PolygonPoints.Add(coord);
                }

                // Altitude parser
                int altFloor = 0;
                int altCeiling = 0;
                AltType altType = AltType.ClampToGround;
                string[] altComponents = this.HelpText.Split(new char[] {'-'});
                if(altComponents.Length == 1)
                {
                    // If length is 1, the user has specified a single altitude (i.e. 5000MSL or 300AGL)
                    // We create the airspace region from the surface to this altitude
                    GenerateAltitude(this.HelpText, ref altType, ref altCeiling);
                    newRegion.AltitudeFloor = 0;
                    newRegion.AltitudeCeiling = altCeiling;
                    newRegion.AltitudeType = altType;
                }
                else
                {
                    GenerateAltitude(altComponents[0], ref altType, ref altFloor);
                    GenerateAltitude(altComponents[1], ref altType, ref altCeiling);
                    newRegion.AltitudeFloor = altFloor;
                    newRegion.AltitudeCeiling = altCeiling;
                    newRegion.AltitudeType = altType;
                }
                
                return newRegion;
            }
            //return null;
        }

        private void GenerateAltitude(string altString, ref AltType typeAltitude, ref int alt)
        {
            // Convert to uppercase and remove whitespace.
            altString = altString.ToUpper();
            altString = altString.Replace(" ", "");

            // This flag will be set true if we find the letters "FL."  This lets later code
            // know to multiply the altitude by 100.
            bool usingFL = false;

            // Figure out what kind of altitude we're talking about.  M or MSL = MSL, a or AGL = AGL,
            // FL = flight level, SFC = 0AGL, anything else is clamp to ground.
            if (altString.Contains("M"))
                typeAltitude = AltType.FixedMSL;
            else if (altString.Contains("FL"))
            {
                typeAltitude = AltType.FixedMSL;
                usingFL = true;
            }
            else if (altString.Contains("A"))
                typeAltitude = AltType.FixedAGL;
            else if (altString.Contains("SFC"))
            {
                typeAltitude = AltType.FixedAGL;
                altString = "0";
            }
            else
                typeAltitude = AltType.ClampToGround;

            // Dump any letters from the string.  At this point, all that's left should be a numeric
            // altitude in string format.
            altString = altString.Replace("MSL", "");
            altString = altString.Replace("AGL", "");
            altString = altString.Replace("M", "");
            altString = altString.Replace("A", "");
            altString = altString.Replace("FL", "");

            // Convert this to a numeric altitude.  if we can't convert, default to ClampToGround
            try
            {
                alt = System.Convert.ToInt32(altString);
                if (usingFL == true)
                    alt *= 100;
            }
            catch
            {
                alt = 0;
                typeAltitude = AltType.ClampToGround;
            }
            return;            
        }        


        // This is a legacy function, but I'm keeping it because I may use it again.  Originally, I envisioned
        // letting the user decide which FalconView fields get converted to which PFPSGET fields.
        private string GetTextField(string source)
        {
            if (source == "FalconView: Text")
                return this.text;
            else if (source == "FalconView: ToolTipText")
                return this.tooltipText;
            else if (source == "FalconView: Help Text")
                return this.helpText;
            else if (source == "FalconView: Comment")
                return this.comment;
            else if (source == "FalconView: Label")
                return this.label;
            else if (source == "Autonumber")
                return this.ID.ToString();
            else
                return "";
        }
        #endregion
    }    
}
