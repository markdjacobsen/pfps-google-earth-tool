/********************************************************************************/
/* Class:   GET.Core.PFPS.DrawingFileImportOptions()                            */
/*                                                                              */
/* This class describes all the options that will be used to import a new       */
/* FalconView draing file.  One of these objects is passed to                   */
/* DrawingFile.Load() to describe how the import will work.  One of these       */
/* objects can be code-generated, or the DrawingFileImportOptions form can be   */
/* used in the front-end PFPS Google Earth Tool application.                    */
/*                                                                              */
/* Public Properties:                                                           */
/*                                                                              */
/* Methods:                                                                     */
/*                                                                              */
/*                                                                              */
/*                                                                              */
/********************************************************************************/


using System;
using System.Collections.Generic;
using System.Text;
using GET.Core.Objects;

namespace GET.Core.PFPS
{
    public class DrawingFileImportOptions
    {
        #region Private Member Data        
        
        private string filename;
        private int defaultAltitudeCeiling;
        private int defaultAltitudeFloor;
        private AltType defaultAltitudeType;
        private Folder folderOptions = new Folder();
        private bool generatePlacemarks = true;

        private string sourceAltitude = "FalconView: Help Text";
        private string sourceDescription = "FalconView: Comment";        
        private string sourceName = "FalconView: Text";
        private string sourceSnippet = "FalconView: ToolTipText";
        private string sourceStyle = "FalconView Style Options";        
        #endregion

        #region Public Properties
        public string Filename
        {
            get { return filename; }
            set { filename = value; }
        }
        public int DefaultAltitudeCeiling
        {
            get { return defaultAltitudeCeiling; }
            set { defaultAltitudeCeiling = value; }
        }
        public int DefaultAltitudeFloor
        {
            get { return defaultAltitudeFloor; }
            set { defaultAltitudeFloor = value; }
        }
        public AltType DefaultAltitudeType
        {
            get { return defaultAltitudeType; }
            set { defaultAltitudeType = value; }
        }
        public Folder FolderOptions
        {
            get { return folderOptions; }
            set { folderOptions = value; }
        }
        public bool GeneratePlacemarks
        {
            get { return generatePlacemarks; }
            set { generatePlacemarks = value; }
        }
        public string SourceAltitude
        {
            get { return sourceAltitude; }
            set { sourceAltitude = value; }
        }
        public string SourceDescription
        {
            get { return sourceDescription; }
            set { sourceDescription = value; }
        }
        public string SourceName
        {
            get { return sourceName; }
            set { sourceName = value; }
        }
        public string SourceSnippet
        {
            get { return sourceSnippet; }
            set { sourceSnippet = value; }
        }
        public string SourceStyle
        {
            get { return sourceStyle; }
            set { sourceStyle = value; }
        }                                
        #endregion

        #region Methods
        #endregion
    }
}
