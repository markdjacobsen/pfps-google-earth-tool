/********************************************************************************/
/* Class:   GET.Core.PFPS.RouteFile                                             */
/*                                                                              */
/* This class is used for loading and converting .RTE files.  It uses the       */
/* PFPS RouteServer to load the specified route file into an MPRoute object,    */
/* then can convert into a GET Route object.  This file depends on the          */
/* RSCOMLib for RouteServer support.                                            */
/*                                                                              */
/* Member Data:                                                                 */
/*  string routeName            The name (not including the path) of the route  */
/*  MPRoute mpRoute             An "MPRoute" object from the RouteServer SDK.   */
/*                              Contains the route after Import() is called.    */
/*                                                                              */
/* Methods:                                                                     */
/*  Import(string filename)     Imports the specified file into "mpRoute"       */
/*  Route Convert()             Converts the MPRoute to a GET Route object and  */
/*                              returns the result.                             */
/*  Close()                     Close the RouteServer connection to the open    */
/*                              route                                           */
/********************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using GET.Core;
using GET.Core.Objects;
using RSCOMLib;

namespace GET.Core.PFPS
{
    public class RouteFile
    {
        #region Private Member Data
        MPRoute mpRoute = new MPRoute();
        string routeName;  
        #endregion

        #region Public Properties
        #endregion

        #region Methods
        public bool Import(string filename)
        {
            // Connect to the RouteServer
            MPClient client = new MPClient();
            try
            {
                client.ConnectToServer("PFPSGoogleEarthTool");
            }
            catch
            {
                return false;
            }

            // Import the route
            try
            {
                mpRoute = client.get_RequestRoute(filename);
            }
            catch
            {
                client.Close();
                return false;
            }
            
            // Close the connection to the RouteServer
            client.Close();                       

            // Break out the route name
            string[] sections = filename.Split(new char[] { '\\'} );
            routeName = sections[sections.Length - 1];

            return true;
        }

        public Route Convert()
        {            
            Route newRoute = new Route();

            MPWaypointSet waypointSet = (MPWaypointSet)mpRoute.Waypoints;
            newRoute.Name = routeName;            
            foreach(MPWayPoint oldPoint in waypointSet)
            {
                Waypoint newPoint = new Waypoint();
                newPoint.Name = oldPoint.RSPoint.Name;
                newPoint.Coordinates.Assign(oldPoint.RSPoint.Latitude, oldPoint.RSPoint.Longitude);
                newRoute.Points.Add(newPoint);                
            }            
            return newRoute;
        }

        public void Close()
        {
            if(mpRoute == null)
                return;
            mpRoute.Close(0,1);
        }
        #endregion

    }
}
