using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Design;
using System.Text;
using GET.Core;
using GET.Core.Objects;

namespace GET.Core.PFPS.Internal
{
    public class ThreatObject
    {
        #region Private Member Data
        private int id;
        private int correlation;
        private string milstd_id;
        private double latitude;
        private double longitude;
        private string datetime;
        private string official_name;
        private string approved_nickname;

        // Probability ellipse
        private float ellipse_angle_deg;
        private float ellipse_smaj_nmi;
        private float ellipse_smin_nmi;

        // Detection ring
        private bool detection_agl;        
        private double detection_range;
        private int detection_height;
        private bool show_detection_ring;        
        
        // Engagement ring        
        private bool engagement_agl;        
        private double engagement_range;
        private int engagement_height;
        private bool show_engagement_ring;              

        private string icon_filename;        
        private string information;
        private bool show_threat;
        private bool show_ellipse;
        private bool enable_edit;
        private string source;
        private short ob_type;
        private string label_text_left;
        private string label_text_right;        
        #endregion

        #region Public Properties
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public int Correlation
        {
            get { return correlation; }
            set { correlation = value; }
        }
        public string Milstd_id
        {
            get { return milstd_id; }
            set { milstd_id = value; }
        }
        public double Latitude
        {
            get { return latitude; }
            set { latitude = value; }
        }
        public double Longitude
        {
            get { return longitude; }
            set { longitude = value; }
        }
        public string Datetime
        {
            get { return datetime; }
            set { datetime = value; }
        }
        public string Icon_filename
        {
            get { return icon_filename; }
            set { icon_filename = value; }
        }
        public string Official_name
        {
            get { return official_name; }
            set { official_name = value; }
        }
        public string Approved_nickname
        {
            get { return approved_nickname; }
            set { approved_nickname = value; }
        }
        public float Ellipse_angle_deg
        {
            get { return ellipse_angle_deg; }
            set { ellipse_angle_deg = value; }
        }
        public float Ellipse_smaj_nmi
        {
            get { return ellipse_smaj_nmi; }
            set { ellipse_smaj_nmi = value; }
        }
        public float Ellipse_smin_nmi
        {
            get { return ellipse_smin_nmi; }
            set { ellipse_smin_nmi = value; }
        }
        public bool Detection_agl
        {
            get { return detection_agl; }
            set { detection_agl = value; }
        }
        public double Detection_range
        {
            get { return detection_range; }
            set { detection_range = value; }
        }
        public int Detection_height
        {
            get { return detection_height; }
            set { detection_height = value; }
        }
        public bool Show_detection_ring
        {
            get { return show_detection_ring; }
            set { show_detection_ring = value; }
        }
        public bool Engagement_agl
        {
            get { return engagement_agl; }
            set { engagement_agl = value; }
        }
        public double Engagement_range
        {
            get { return engagement_range; }
            set { engagement_range = value; }
        }
        public int Engagement_height
        {
            get { return engagement_height; }
            set { engagement_height = value; }
        }
        public bool Show_engagement_ring
        {
            get { return show_engagement_ring; }
            set { show_engagement_ring = value; }
        }
        public string Information
        {
            get { return information; }
            set { information = value; }
        }
        public bool Show_threat
        {
            get { return show_threat; }
            set { show_threat = value; }
        }
        public bool Show_ellipse
        {
            get { return show_ellipse; }
            set { show_ellipse = value; }
        }
        public bool Enable_edit
        {
            get { return enable_edit; }
            set { enable_edit = value; }
        }
        public string Source
        {
            get { return source; }
            set { source = value; }
        }
        public short Ob_type
        {
            get { return ob_type; }
            set { ob_type = value; }
        }
        public string Label_text_left
        {
            get { return label_text_left; }
            set { label_text_left = value; }
        }
        public string Label_text_right
        {
            get { return label_text_right; }
            set { label_text_right = value; }
        }
        #endregion

        #region Methods
        public GETObject Convert()
        {
            Threat newThreat = new Threat();
            newThreat.Coordinates.Assign(this.Latitude, this.Longitude);
            newThreat.DetectionCeiling = this.detection_height;
            if (newThreat.DetectionCeiling == 0)
                newThreat.DetectionCeilingType = AltType.ClampToGround;
            else
            {
                if (this.Detection_agl == true)
                    newThreat.DetectionCeilingType = AltType.FixedAGL;
                else
                    newThreat.DetectionCeilingType = AltType.FixedMSL;
            }
            newThreat.DetectionRange = this.Detection_range;
            newThreat.EllipseAngle = (int)this.Ellipse_angle_deg;
            newThreat.EllipseMajorAxis = this.Ellipse_smaj_nmi;
            newThreat.EllipseMinorAxis = this.Ellipse_smin_nmi;
            newThreat.EngagementCeiling = this.Engagement_height;
            if (newThreat.EngagementCeiling == 0)
                newThreat.EngagementCeilingType = AltType.ClampToGround;
            else
            {
                if (this.Engagement_agl == true)
                    newThreat.EngagementCeilingType = AltType.FixedAGL;
                else
                    newThreat.EngagementCeilingType = AltType.FixedMSL;
            }
            newThreat.EngagementRange = this.Engagement_range;
            newThreat.Name = this.Id + ". " + this.Official_name;            
            newThreat.ShowVisible = this.Show_threat;
            newThreat.StyleOptions.IconPath = this.Icon_filename;
            newThreat.StyleOptions.FillColor = Color.DarkRed;
            newThreat.StyleOptions.LineColor = Color.Red;

            newThreat.Description = "<b>APPROVED NICKNAME: </b>" + this.Approved_nickname + "<br>";
            newThreat.Description += "<b>CORRELATION CODE: </b>" + this.Correlation + "<br>";
            newThreat.Description += "<b>INFORMATION: </b>" + this.Information + "<br>";
            newThreat.Description += "<b>MILSTD ID: </b>" + this.Milstd_id + "<br>";
            newThreat.Description += "<b>OB TYPE: </b>" + this.Ob_type + "<br>";
            newThreat.Description += "<b>OFFICIAL NAME: </b>" + this.Official_name + "<br><br>";
            
            return newThreat;            
        }
        #endregion
    }
}
